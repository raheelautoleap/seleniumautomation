package com.autoleap.Pageobject;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CreateCustomerPage {
	
	
	WebDriver driver;
	
	public CreateCustomerPage(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}
    
	
	@FindBy(xpath ="//*[@id=\"navbarSupportedContent\"]/ul/li[4]/a/i")
	WebElement Listlink;
	
	@FindBy(xpath ="/html/body/app-dashboard/div/main/app-lists-view/div/div/app-customer-list/div/div/div/div/div/div[2]/button")
	WebElement Custbtn;
	
	@FindBy(xpath ="/html/body/p-dynamicdialog/div[2]/div/app-add-dialog/div/div/div[2]/span[1]/app-customer-add/div/form/div/div[1]/div[1]/app-input/div/div[1]/input")
	WebElement txtfname;
	
	@FindBy(xpath ="/html/body/p-dynamicdialog/div[2]/div/app-add-dialog/div/div/div[2]/span[1]/app-customer-add/div/form/div/div[1]/div[2]/app-input/div/div[1]/input")
	WebElement txtlname;
	
	@FindBy(xpath ="/html/body/p-dynamicdialog/div[2]/div/app-add-dialog/div/div/div[2]/span[1]/app-customer-add/div/form/div/div[2]/div[1]/app-input/div/div[1]/input")
	WebElement txtmobile;
	
	@FindBy(xpath ="/html/body/p-dynamicdialog/div[2]/div/app-add-dialog/div/div/div[2]/span[1]/app-customer-add/div/form/div/div[2]/div[2]/app-input/div/div[1]/input")
	WebElement txtemail;
	
	@FindBy(xpath ="/html/body/p-dynamicdialog/div[2]/div/app-add-dialog/div/div/div[3]/input[2]")
	WebElement btnsave;
	
	public void setFname(String fname)
	{
		txtfname.sendKeys(fname);
	}
	
	public void setLname(String lname)
	{
		txtlname.sendKeys(lname);
	}
	
	public void setEmailaddress(String email)
	{
		txtemail.sendKeys(email);
	}
	
	public void setMobile(String mobile)
	{
		txtmobile.sendKeys(mobile);
	}
	
	public void clicSavebtn()
	{
		btnsave.click();
	}
	
	public void clicCustomerbtn()
	{
		Custbtn.click();
	}
	
	public void clicklist()
	{
		Listlink.click();
	}
	
	
}
