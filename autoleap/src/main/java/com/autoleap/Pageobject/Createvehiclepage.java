package com.autoleap.Pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Createvehiclepage {
	
	WebDriver driver;

	public Createvehiclepage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	@FindBy(xpath = "/html/body/p-dynamicdialog[2]/div[2]/div[2]/app-add-successfully-dialog/div/div/div[1]")
	WebElement txtsearch;

	public void Addvehicle() {
		txtsearch.click();
	}

}
