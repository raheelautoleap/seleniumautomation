package com.autoleap.Pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class VerifyCustomerPage {

	WebDriver driver;

	public VerifyCustomerPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	@FindBy(xpath = "/html/body/app-dashboard/div/main/app-lists-view/div/div/app-customer-list/div/div/div/div/div/div[1]/app-auto-complete/div/div/div[1]")
	WebElement txtsearch;

	public void setSearchName(String searchname) {
		txtsearch.sendKeys(searchname);
	}

}
