package com.autoleap.Pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Homepage {

WebDriver driver;
	
	public Homepage(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}
    
	
	@FindBy(xpath ="/html/body/app-login/div/main/div/div/div/div/div/form/div[2]/app-input/div/div[1]/input")
	WebElement txtEmailaddress;
	
	@FindBy(xpath="/html/body/app-login/div/main/div/div/div/div/div/form/div[3]/app-input/div/div[1]/input")
	WebElement txtPassword ;
	
	@FindBy(xpath="/html/body/app-login/div/main/div/div/div/div/div/form/div[5]/div/button")
	WebElement btnLogin;
	
	public void setEmailaddress(String username)
	{
		txtEmailaddress.sendKeys(username);
	}
	
	public void setPasswoard(String userPass)
	{
		txtPassword.sendKeys(userPass);
	}
	
	public void clickBtn()
	{
		btnLogin.click();
	}
}
