package com.autoleap.Testcase;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;

import com.github.javafaker.Faker;

import com.autopeal.DataProviderFactory.DataProviderFactory;
import com.autoleap.DataProvider.RandomDataProvider;
import com.autoleap.Pageobject.CreateCustomerPage;
import com.autoleap.Pageobject.Createvehiclepage;
import com.autoleap.Pageobject.Homepage;
import com.autoleap.Pageobject.VerifyCustomerPage;
import com.autoleap.Testcase.BaseClass;
import com.autoleap.Utility.RandomData;



public class CreateCustomer extends BaseClass {
	
	
		
	@Test
	public void TestCreateCustomer() throws IOException, InterruptedException
	{
		System.out.println("inside CreateCustomer");
		
		// call Random Data Method
		
		RandomData.updateproFile();
		
		// login into Application
		Homepage objhomepage = new Homepage(driver);
		System.out.println("after Create Home Page object");
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		objhomepage.setEmailaddress(DataProviderFactory.getRandomDataProperty().getValue("userEmailaddress"));
		objhomepage.setPasswoard(DataProviderFactory.getRandomDataProperty().getValue("Password"));
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		objhomepage.clickBtn();
		
		System.out.println("Click on Submit Button");
		
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		//// End login ////
		
		//// Create customer /////
		CreateCustomerPage objCC = new CreateCustomerPage(driver);
		System.out.println("after Create Customer Page object");
		
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		objCC.clicklist();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		objCC.clicCustomerbtn();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		objCC.setFname(DataProviderFactory.getRandomDataProperty().getValue("Firstname"));
		objCC.setLname(DataProviderFactory.getRandomDataProperty().getValue("lastname"));
		objCC.setMobile(DataProviderFactory.getRandomDataProperty().getValue("mobile"));
		objCC.setEmailaddress(DataProviderFactory.getRandomDataProperty().getValue("Emailaddress"));
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		objCC.clicSavebtn();
		
		Thread.sleep(5000);
		
		
		Createvehiclepage objVC = new Createvehiclepage(driver);
		System.out.println("after Create Create vehicle Page object");
		
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		
		objVC.Addvehicle();
		
		/*
		/// Verify Customer 
		VerifyCustomerPage objVC = new VerifyCustomerPage(driver);
		System.out.println("after Create Verify Customer Page object");
		
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		
		objVC.setSearchName(DataProviderFactory.getRandomDataProperty().getValue("Firstname"));
		*/
		
			
	}
	
}