package com.autoleap.Utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Properties;
import java.util.Random;

import com.github.javafaker.Faker;

public class RandomData {

public static void updateproFile() throws IOException {

		
		Faker faker = new Faker();

		String Firstname = faker.name().firstName() + faker.name().lastName();
		String LastName = faker.name().lastName();
		String MobileNo = genphonenumber();
		String Emailaddress = Firstname  + "@gmail.com";
		
		System.out.println("Fname :" + Firstname + " LastName :" + LastName);
		
		
		FileOutputStream fileOut = null;
        FileInputStream fileIn = null;
        
       
            Properties configProperty = new Properties();
              
            File file = new File("./configs/RandomData2.properties");
            fileIn = new FileInputStream(file);
            configProperty.load(fileIn);
            configProperty.setProperty("Firstname", Firstname);
            configProperty.setProperty("lastname", LastName);
    		configProperty.setProperty("Emailaddress", Emailaddress);
    		configProperty.setProperty("mobile", MobileNo);
    		
            fileOut = new FileOutputStream(file);
            configProperty.store(fileOut, null);
            fileIn.close();
           

	}

	public static String genphonenumber() {

		// create object of Random class
		Random rand = new Random();

		// generate random number as phone number
		int num1 = (rand.nextInt(7) + 1) * 100 + (rand.nextInt(8) * 10) + rand.nextInt(8);
		int num2 = rand.nextInt(743);
		int num3 = rand.nextInt(10000);

		// set format if random number is zero
		DecimalFormat df3 = new DecimalFormat("000"); // 3 zeros
		DecimalFormat df4 = new DecimalFormat("0000"); // 4 zeros

		// concatenate all random number as per phone number format
		String phoneNumber = df3.format(num1) + "-" + df3.format(num2) + "-" + df4.format(num3);

		// return phone number
		return phoneNumber;

	}

}
