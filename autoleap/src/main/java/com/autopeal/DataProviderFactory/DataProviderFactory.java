package com.autopeal.DataProviderFactory;

import com.autoleap.DataProvider.RandomDataProvider;

public class DataProviderFactory {
	
	public static RandomDataProvider getRandomDataProperty() {

		return new RandomDataProvider();
	}

}
